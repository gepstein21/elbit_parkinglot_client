import { Injectable } from '@angular/core';
import { Subject, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';

/*
@Injectable({
  providedIn: 'root'
})*/
export class BaseService<T> {

  baseSubject = new Subject<T>();
  data : T = null;
  public servicePath : string;
  
  constructor(public httpClient : HttpClient) { }

  getData(Id? : string) : Observable<T>
  {
    let serviceParam : string;
    serviceParam = Id ? this.servicePath + Id : this.servicePath;  
    return this.httpClient.get<T>(serviceParam)
      .pipe(
        tap(res => 
          {
            this.baseSubject.next(res);
            this.data = res;
          }),
          catchError(error => throwError(error))
      );
  }

  postData(objectToPost : T) : Observable<boolean>
  {
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json'), withCredentials: true };
    return this.httpClient.post<boolean>(this.servicePath, {objectToPost}, config);
  }

  deleteData(Id? : string) : Observable<boolean>
  {
    let serviceParam : string;
    serviceParam = Id ? this.servicePath + Id : this.servicePath;  
    return this.httpClient.delete<boolean>(serviceParam);
  }
}
