import { CarModel } from './CarModel';

export class Car
{
    //id: number; 
    plateNumber : string;    
    modelID : /*CarModel*/number;
    typeModel : string;
    yearModel : number;
    numberOfKM : number;
}