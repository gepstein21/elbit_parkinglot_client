import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { CarModel } from '../Models/CarModel';
import { servicesPath } from '../app.config';
import { CarLotCar } from '../Models/carLotCar';
import { BaseService } from './base.service';
import { tap, catchError } from 'rxjs/operators';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CarsService implements Resolve<CarLotCar[]> /*extends BaseService<CarLotCar[]>*/ {

  servicePath : string;
  carsModels : CarModel[];
  cars : CarLotCar[] = [];
  carsSubject = new Subject<CarLotCar[]>();
  oncarsSubjectChange : Observable<CarLotCar[]> = this.carsSubject.asObservable();


  constructor(private http: HttpClient) 
  {
    //super(http);
    this.servicePath = servicesPath.CarsData;
  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CarLotCar[]> {
    return this.getCars();
  }
  getCarsModels() : Observable<CarModel[]>
  {
    let url = servicesPath.CarsModel; 
    return this.http.get<CarModel[]>(url);
  }

  addCar(car : CarLotCar) : Observable<CarLotCar[]>
  {
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json'), withCredentials: true };
    return this.http.post<CarLotCar[]>(this.servicePath, car,  config)
    .pipe(
      tap(res => 
        {
          this.carsSubject.next(res);
          this.cars = res;
        }),
        catchError(error =>  throwError(error))
    );
  }
  updateCar(plateNumber : string, car : CarLotCar)
  {
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json'), params: new HttpParams().set('plateNumber',plateNumber), withCredentials: true };
    return this.http.put<CarLotCar[]>(this.servicePath, car,  config)
    .pipe(
      tap(res => 
        {
          this.carsSubject.next(res);
          this.cars = res;
        }),
        catchError(error =>  throwError(error))
    );
  }

  getCars() : Observable<CarLotCar[]>
  {
    return this.http.get<CarLotCar[]>(this.servicePath).pipe(
      tap(res => 
        {
          this.carsSubject.next(res);
          this.cars = res;
        }),
        catchError(error => throwError(error))
    );
  }

  delete(plateNumber : string):Observable<CarLotCar[]>
  {
    const config = { headers: new HttpHeaders().set('Content-Type', 'application/json'), params: new HttpParams().set('plateNumber',plateNumber), withCredentials: true };
    return this.http.delete<CarLotCar[]>(this.servicePath, config)
    .pipe(
      tap(res => 
        {
          this.carsSubject.next(res);
          this.cars = res;
        }),
        catchError(error =>  throwError(error))
    );
  }

}
