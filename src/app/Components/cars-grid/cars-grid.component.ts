import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UpdateCarComponent } from '../update-car/update-car.component';
import { CarLotCar } from 'src/app/Models/carLotCar';
import { CarsService } from 'src/app/Services/cars.service';

@Component({
  selector: 'app-cars-grid',
  templateUrl: './cars-grid.component.html',
  styleUrls: ['./cars-grid.component.scss']
})
export class CarsGridComponent implements OnInit {

  numberOfCars : number;
  cars : CarLotCar[];
  constructor(private modalService: NgbModal, private carsSrv : CarsService) { }

  ngOnInit(): void {
    this.carsSrv.oncarsSubjectChange.subscribe(result => 
      {
        this.cars = result;
      });
     this.cars = this.carsSrv.cars; 
  }
  openModalToUpdateCar(car : CarLotCar)
  {
    let activeModal : NgbActiveModal;
    const modalRef = this.modalService.open(UpdateCarComponent, { size: 'xl' });
    modalRef.componentInstance.carToEdit = car;
    modalRef.componentInstance.activeModal = activeModal;
    modalRef.componentInstance.formWasSavedAsModal.subscribe(result => modalRef.close());
  }

  get carsExists()
  {
    return this.cars && this.cars.length > 0;
  }

  delete(plateNumber : string)
  {
    if(confirm(`Are you sure you want to delete Car with Plate Number ${plateNumber} ?`))
    {
      this.carsSrv.delete(plateNumber).subscribe();
    }
  }

  getModelName(modelId : number) : string
  {
    return this.carsSrv.carsModels.find(car => car.id == modelId).name;
  }
}
