import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import {HttpClientModule, HttpClient} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateCarComponent } from './Components/update-car/update-car.component';
import { CarsGridComponent } from './Components/cars-grid/cars-grid.component';
import { CarsService } from './Services/cars.service';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { pipe } from 'rxjs';

export function init_Module(carsSrv : CarsService)
{
  return () => {
    return new Promise((resolve) => {
      pipe
      {
        carsSrv.getCarsModels().subscribe(result => 
          {
            carsSrv.carsModels = result;
            resolve();
          }
        );
      }
    });
  };
}





@NgModule({
  declarations: [
    AppComponent,
    CarsGridComponent,
    UpdateCarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    ReactiveFormsModule,
    AppRoutingModule,
    NgbModule, 
    HttpClientModule,
    RouterModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER, useFactory: init_Module, deps: [CarsService], multi: true
    }], 
  bootstrap: [AppComponent]
})
export class AppModule { }
