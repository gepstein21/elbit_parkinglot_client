import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarsGridComponent } from './Components/cars-grid/cars-grid.component';
import { UpdateCarComponent } from './Components/update-car/update-car.component';
import { CarsService } from './Services/cars.service';



const routes: Routes =
[
  { path: 'CarUpdate', component: UpdateCarComponent },
  { path: 'CarsLot', component: CarsGridComponent, resolve: {carsSrv: CarsService } },
  { path: '',   redirectTo: '/CarsLot', pathMatch: 'full' },
  { path: '**', component: CarsGridComponent } 
] 


@NgModule({
  imports: [RouterModule.forRoot(routes/*,{useHash: true}*/)],
  exports: [],
})

export class AppRoutingModule { }
