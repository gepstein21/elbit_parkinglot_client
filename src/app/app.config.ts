import { environment } from 'src/environments/environment';

export const apiUrl = environment.apiUrl;
export const servicesPath =
{
    CarsModel: "../assets/carsModels.json", 
    CarsData:  `${apiUrl}/Cars`
}