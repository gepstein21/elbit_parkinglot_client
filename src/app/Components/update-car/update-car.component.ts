import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CarModel } from 'src/app/Models/CarModel';
import { CarsService } from 'src/app/Services/cars.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CarLotCar } from 'src/app/Models/carLotCar';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-update-car',
  templateUrl: './update-car.component.html',
  styleUrls: ['./update-car.component.scss']
})

export class UpdateCarComponent implements OnInit {

  @Input() carToEdit : CarLotCar;
  @Output() formWasSavedAsModal = new EventEmitter();
  carDetailsForm :FormGroup ; 
  submitText : string;
  UpdateAddString : string;
  carsModels : CarModel[]; 
  wasCarAdded : boolean = false;
  ModelId : number;
  constructor(private carSrv : CarsService, private fb : FormBuilder) { }

  ngOnInit(): void {
  
    this.submitText = this.carToEdit ? "Update" : "Add";
    this.UpdateAddString = this.carToEdit ? "Car was edited succesfully" : "Car Was added succesfully";
    this.carsModels = this.carSrv.carsModels;

    if (this.carToEdit)
    {
      this.carDetailsForm = this.fb.group(
        {
          modelID:[this.carToEdit.modelID],
          PlateNumber:[this.carToEdit.plateNumber],
          TypeModel:[this.carToEdit.typeModel], 
          YearModel:[this.carToEdit.yearModel, [Validators.required, Validators.pattern("^[0-9]*$")]],
          NumberOfKM:[this.carToEdit.numberOfKM, [Validators.required,Validators.pattern("^[0-9]*$")]], 
          WantedPrice:[this.carToEdit.wantedPrice]
        });
    }
    else{
      this.carDetailsForm = this.fb.group(
        {
          modelID:[''],
          PlateNumber:[''],
          TypeModel:[''], 
          YearModel:['', [Validators.required, Validators.pattern("^[0-9]*$")]],
          NumberOfKM:['', [Validators.required,Validators.pattern("^[0-9]*$")]], 
          WantedPrice:['']
        });
    }
  }
  
  addUpdateCar()
  {
    let car = this.carDetailsForm.value as CarLotCar;
    if (!this.carToEdit)
    {
      car.receivingDate = new Date();
      this.carSrv.addCar(this.carDetailsForm.value as CarLotCar)
      .subscribe(result => 
        {
          this.wasCarAdded = true;
        }
        
      );
    }
    else{
      this.carSrv.updateCar(this.carDetailsForm.value.PlateNumber, this.carDetailsForm.value as CarLotCar)
      .subscribe(result => 
        {
          this.wasCarAdded = true;
          this.formWasSavedAsModal.emit("Closed");
        }
        
      );
    }
  }
 
}
